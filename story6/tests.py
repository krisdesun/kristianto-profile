from django.test import TestCase, Client
from django.urls import reverse, resolve
from story6.views import kegiatan, tambahkegiatan, tambahpeserta
from .models import Kegiatan, Peserta
from .forms import Formkegiatan, Formpeserta
# Create your tests here. 
#get manggil html
#resolve func dipanggil
#
class storyTest(TestCase):
    def setup(self):
        self.client = Client

    def test_kegiatan_resolv(self):
        url = reverse('kegiatan')
        self.assertEquals(resolve(url).func,kegiatan)


    def test_kegiatan_exists(self):
        response = self.client.get(reverse('kegiatan'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,'kegiatan.html')

    def test_kegiatan_template(self):
        response = self.client.get('/kegiatan/')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,'kegiatan.html')

    def test_tambahkegiatan_resolv(self):
        url = reverse('tambahkegiatan')
        self.assertEquals(resolve(url).func,tambahkegiatan)

    def test_tambahkegiatan(self):
        #ulr = self.client.post(data ='adsfadsf' )
        #self.assertEquals(resolve(url),)
        activity = Kegiatan.objects.create(nama='adfad')
        self.assertNotEqual(Kegiatan.objects.all().count(),0)
        self.assertEquals(str(Kegiatan.objects.get(pk=1)),'adfad')

    def test_tambahpeserta(self):
        #ulr = self.client.post(''data ='adsfadsf' )
        #self.assertEquals(resolve(url),)
        activity = Kegiatan.objects.create(nama='adfad')
        activity2 = Peserta.objects.create(nama='saya',kegiatan=activity)
        self.assertEquals(str(Peserta.objects.get(pk=1)),'saya')

    def test_story6_post(self):
        form = Formkegiatan(data={'nama':"test"})
        self.assertTrue(form.is_valid())
        response_post = self.client.post('/kegiatan/tk', {'nama':"dyhd"})
        self.assertEqual(response_post.status_code, 302)

        #response= Client().get('/story6/')
        #html_response = response.content.decode('utf8')
        #self.assertIn(test, html_response)
    def test_app(self):
        found = resolve('/kegiatan/tk')
        self.assertEqual(found.func, tambahkegiatan)






"""
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, detail
from .models import Acara, Peserta
from .forms import AcaraForm

# Create your tests here.
class Story6UnitTest(TestCase):

    def test_story6_url_is_exist(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)
    
    def test_story6_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    # def test_story6_detail_url_is_exist(self):
    #     response = Client().get('/story6/1/')
    #     self.assertEqual(response.status_code, 200)

    def test_story6_using_index_func(self):
        found = resolve('/story6/')
        self.assertEqual(found.func, index)
    
    def test_story6_using_detail_func(self):
        found = resolve('/story6/1/')
        self.assertEqual(found.func, detail)

    def test_model_can_create_new_acara(self):
        # Creating a new activity
        new_activity = Acara.objects.create(name='mengerjakan lab ppw')

        # Retrieving all available activity
        counting_all_available_acara = Acara.objects.all().count()
        self.assertEqual(counting_all_available_acara, 1)
    
    def test_model_can_create_new_peserta(self):
        # Creating a new activity
        new_peserta = Peserta.objects.create(name='bob')

        # Retrieving all available activity
        counting_all_available_peserta = Peserta.objects.all().count()
        self.assertEqual(counting_all_available_peserta, 1)

    def test_form_acara_input_has_placeholder_and_css_classes(self):
        form = AcaraForm()
        # self.assertIn('class="acara-form-input', form.as_p())
        self.assertIn('id="id_name"', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = AcaraForm(data={'name':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )

    def test_story6_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/story6/', {'name':test})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/story6/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
    
    def test_story6_detail_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/story6/', {'name':test})
        self.assertEqual(response_post.status_code, 200)

        response_post = Client().post('/story6/1/', {'name':test})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/story6/1/',)
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_story6_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('story6/', {'name': ''})
        self.assertEqual(response_post.status_code, 404)

        response= Client().get('story6/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_story6_detail_url_is_exist(self):
        test = 'Anonymous'
        response_post = Client().post('/story6/', {'name':test})
        self.assertEqual(response_post.status_code, 200)

        response_post = Client().post('/story6/1/', {'name':test})
        self.assertEqual(response_post.status_code, 200)

        response = Client().post('/peserta/1/delete/')
        self.assertEqual(response.status_code, 302)

        counting_all_available_peserta = Peserta.objects.all().count()
        self.assertEqual(counting_all_available_peserta, 0)

    def test_story6_delete_url_is_exist(self):
        response = Client().get('/peserta/1/delete/')
        self.assertEqual(response.status_code, 302)

r"""
