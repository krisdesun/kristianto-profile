from django.urls import path 
from .views import kegiatan, tambahkegiatan, tambahpeserta

urlpatterns = [
	path('kegiatan/', kegiatan, name = 'kegiatan'),
	path('kegiatan/tk', tambahkegiatan, name = 'tambahkegiatan'),
	path('kegiatan/tp/<int:pk>', tambahpeserta, name = 'tambahpeserta')



]