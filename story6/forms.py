from django import forms

class Formkegiatan(forms.Form):
	nama = forms.CharField(max_length=50,
							widget=forms.TextInput(attrs={
								'class':'form-control',
								'placeholder':'tambah kegiatan'
								}))

class Formpeserta(forms.Form):
	nama = forms.CharField(max_length=50,
							widget=forms.TextInput(attrs={
								'class':'form-control',
								'placeholder':'tambah peserta'
								}))
	