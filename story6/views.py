from django.shortcuts import render, redirect
from .models import Kegiatan, Peserta
from .forms import Formkegiatan, Formpeserta

# Create your views here.
def kegiatan(request):
	context={
		"datakegiatan":Kegiatan.objects.all(), 
		"datapeserta":Peserta.objects.all(),
		"formkegiatan":Formkegiatan,
		"formpeserta":Formpeserta
	}
	return render(request,"kegiatan.html",context)

def tambahkegiatan(request):
	form = Formkegiatan(request.POST)
	if request.method == 'POST':
		nama = request.POST['nama']
		Kegiatan.objects.create(nama=nama)
		return redirect('kegiatan')

	# if form.is_valid():
	# 	myregister = Kegiatan(nama = form.cleaned_data['nama'])
	# myregister.save()
	# return redirect('kegiatan')

def tambahpeserta(request,pk):
	form = Formpeserta(request.POST)
	if form.is_valid():
		myregister = Peserta(nama = form.cleaned_data['nama'], kegiatan=Kegiatan.objects.get(id=pk) )
	myregister.save()
	return redirect('kegiatan')
	#return render(request,"index_lab1.html")