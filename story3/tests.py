from django.test import TestCase, Client
from django.urls import reverse, resolve
from story3.views import index, project, blog, galeri, story1
# Create your tests here.
class storyTest(TestCase):
	def setup(self):
		self.client = Client

	def test_index_resolv(self):
		url = reverse('index')
		self.assertEquals(resolve(url).func,index)

	def test_project_resolve(self):
		url = reverse('project')
		self.assertEquals(resolve(url).func,project)

	def test_blog_resolve(self):
		url = reverse('blog')
		self.assertEquals(resolve(url).func,blog)

	def test_blog_exists(self):
		response = self.client.get(reverse('blog'))
		self.assertEquals(response.status_code, 200)
		self.assertTemplateUsed(response,'blog.html')
