from django.db import models

# Create your models here. 
class jadwal(models.Model):
	nama = models.CharField(max_length=100)
	dosen = models.CharField(max_length=100)
	sks = models.CharField(max_length=10)
	ruang = models.CharField(max_length=10)
	desc = models.TextField(max_length=100)

	def __str__(self):
		return self.nama
