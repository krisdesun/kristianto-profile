from django.shortcuts import render, redirect 
from django.http import HttpResponse
# Create your views here.
from .forms import FormJadwal, FormBeli 
from .models import jadwal

def index(request):
	return render(request,"index_lab1.html")
def project(request):
	return render(request,"project.html")
def blog(request):
	return render(request,"blog.html")
def galeri(request):
	return render(request,"challenge.html")
def story1(request):
	return render(request,"story1.html")

def jadwalmapel(request):
	return render(request,'jadwal.html',{
			'jdwl':jadwal.objects.all()
		})

def tambahmapel(request):
	form = FormJadwal(request.POST)

	if form.is_valid():
		myregister = jadwal(nama = form.cleaned_data['nama'],dosen = form.cleaned_data['dosen'],sks = form.cleaned_data['sks'],ruang = form.cleaned_data['ruang'],desc = form.cleaned_data['desc'])
		
		myregister.save()
	return redirect('jadwal')

def register(request):
	context =  {
		"form":FormJadwal
	}
	return render(request,'tambahjadwal.html',context)

def detail(request,pk):
	data = jadwal.objects.get(pk=pk)
	context = {
		"mapel":data
	}
	return render(request,'detail.html',context)

def deletejadwal(request, pk):
	if request.method =='POST':
	 	jdwl = jadwal.objects.get(pk=pk)
	 	jdwl.delete()
	return redirect('jadwal')

def beli(request):
	##data = jadwal.objects.get(pk=pk)##barang.objects
	context = {
	##	"data":data,
		"form":FormBeli
	}
	return render(request,"beli.html", context)
"""
def cekharga(request):##request, pk
	if request.method =='POST':
	 	##jdwl = jadwal.objects.get(pk=pk)   class winalton.objects.get
	 	##harga= barang.objects
	return render(request,"beli.html",{"harga":harga})"""