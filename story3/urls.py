from django.urls import path
from .views import index, project, blog, galeri, story1
from .views import jadwalmapel, tambahmapel, register, detail, deletejadwal, beli##cekharga
 
urlpatterns = [
	path('', index, name = 'index'),
    path('project/', project, name = 'project'),
    path('blog/', blog, name = 'blog'),
    path('galeri/', galeri, name = 'galeri'),
    path('story1/',story1, name = 'story1'),
    path('jadwal/', jadwalmapel, name = 'jadwal'),
    path('tambahmapel/', tambahmapel, name = 'tambah'),
    path('jadwal/tambah/', register, name = 'register'),
    path('jadwal/detail/<int:pk>',detail, name = 'detailjadwal'),
    path('jadwal/delete/<int:pk>',deletejadwal,name = 'delete'),

    ##tk
    path('beli/',beli, name = 'beli'),
    ##path('beli/cekharga/<string:kode>',cekharga, name = 'cekharga'),


]
